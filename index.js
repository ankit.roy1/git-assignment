const express=require("express")
const controller=require("./controller")
const app=express()

app.get("/",(req,res)=>{
    res.json({message:"This is the home Page"})
})

app.get("/contact",controller.contact)

app.get("/about",controller.about)

const PORT=5000
app.listen(PORT,()=>{
    console.log(`Server listening on PORT : ${PORT}`)
})